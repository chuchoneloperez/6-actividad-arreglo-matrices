﻿using System;

class Program
{
    static void Main(string[] args)
    {
        int[] array = { 1, 56, 89, 34, 12, 8 };

        // Buscar un número en el arreglo
        int elementoABuscar = 89;
        int indice = BuscarElemento(array, elementoABuscar);
        if (indice != -1)
            Console.WriteLine("El número " + elementoABuscar + " se encuentra en la posición número: " + indice);
        else
            Console.WriteLine("El número " + elementoABuscar + " no se encontró en el arreglo.");

        Console.WriteLine();

        // Ordenar números de menor a mayor
        OrdenarMenorAMayor(array);
        Console.WriteLine("Números ordenados de menor a mayor: ");
        ImprimirArreglo(array);

        Console.WriteLine();

        // Ordenar números de mayor a menor
        OrdenarMayorAMenor(array);
        Console.WriteLine("Números ordenados de mayor a menor: ");
        ImprimirArreglo(array);

        Console.WriteLine();

        // Eliminar un número del arreglo
        int elementoAEliminar = 89;
        array = EliminarElemento(array, elementoAEliminar);
        Console.WriteLine("Arreglo después de eliminar el elemento:");
        ImprimirArreglo(array);
    }

    static int BuscarElemento(int[] array, int elementoABuscar)
    {
        return Array.IndexOf(array, elementoABuscar);
    }

    static void OrdenarMenorAMayor(int[] array)
    {
        Array.Sort(array);
    }

    static void OrdenarMayorAMenor(int[] array)
    {
        Array.Sort(array);
        Array.Reverse(array);
    }

    static int[] EliminarElemento(int[] array, int elementoAEliminar)
    {
        int indice = Array.IndexOf(array, elementoAEliminar);
        if (indice != -1)
        {
            int[] newArray = new int[array.Length - 1];
            Array.Copy(array, 0, newArray, 0, indice);
            Array.Copy(array, indice + 1, newArray, indice, array.Length - indice - 1);
            return newArray;
        }
        else
        {
            Console.WriteLine("El elemento a eliminar no se encontró en el arreglo.");
            return array;
        }
    }

    static void ImprimirArreglo(int[] array)
    {
        foreach (var elemento in array)
        {
            Console.WriteLine(elemento);
        }
    }
}
